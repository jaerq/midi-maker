.data
	key:		.space	1
	replay_dialog:	.asciiz	"Would you like to replay a recording?"
	file_dialog:	.asciiz "Enter the file name to replay:"
	save_dialog:	.asciiz	"Enter the file name to save to:"
	file_name:	.space	9
	save_name:	.space	9
	input_buffer:	.space	100
	output_buffer:	.space	100
	load_color:	.word	0xcafe69
	
.text
#The least significant bit of the Receiver control register (0xFFFF0000) is set to 1 when a keystroke is input into the MMIO Simulator
#This loop checks for a keystroke and jumps to the keypress label for processing the input
main:
#Dialog: Would you like to replay a recording?
#Selection stored in $a0. 0: yes 1: no 2: cancel
	la		$a0, replay_dialog
	li		$v0, 50
	syscall
	move		$s1, $a0	#flag for replay. If s1 is 0, then replay mode is enabled.
	beq		$a0, 1, time_start
	beq		$a0, 2, exit
	
#String Input Dialog: "Enter the file name:"
#String stored in buffer file_name
	la		$a0, file_dialog
	la		$a1, file_name
	li		$a2, 9
	li		$v0, 54
	syscall
	
#open file from file_name buffer
	li		$v0, 13
	la		$a0, file_name
	li		$a1, 0		#open for reading
	li		$a2, 0
	syscall
	move 		$s0, $v0	#file descriptor
	
	blt		$s0, 0, exit	#if $s0 is a negative number, then error opening file
	
#read opened file to input_buffer
	li		$v0, 14	
	move		$a0, $s0		#move file descriptor to a0
	la		$a1, input_buffer	#load address of file buffer
	li		$a2, 100
	syscall

#load byte from input_buffer
#store byte into 0xFFFF0004
#call keypress function
	la		$s2, input_buffer
	li		$t3, 25	#i
replay_loop:
	lb		$t4, ($s2)		#load byte
	beq		$t4, 0, exit		#exit at end of buffer
	
	#loading bar bitmap
	move	$a0, $t3
	li	$a1, 125
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 126
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 127
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 128
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 129
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 130
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 131
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 132
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 133
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 134
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 135
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 136
	lw	$a2, load_color
	move	$a0, $t3
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 137
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 138
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 139
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 140
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 141
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 142
	lw	$a2, load_color
	jal DrawPoint
	
	add	$t3, $t3, 1
	move	$a0, $t3
	li	$a1, 125
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 126
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 127
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 128
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 129
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 130
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 131
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 132
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 133
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 134
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 135
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 136
	lw	$a2, load_color
	move	$a0, $t3
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 137
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 138
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 139
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 140
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 141
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 142
	lw	$a2, load_color
	jal DrawPoint
	add	$t3, $t3, 1
	move	$a0, $t3
	li	$a1, 125
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 126
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 127
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 128
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 129
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 130
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 131
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 132
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 133
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 134
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 135
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 136
	lw	$a2, load_color
	move	$a0, $t3
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 137
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 138
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 139
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 140
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 141
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 142
	lw	$a2, load_color
	jal DrawPoint
	
	add		$t3, $t3, 1
	add		$s2, $s2, 1		#increment to the next byte
	beq		$t4, 0x00000020, replay_pause	#if a space character is encountered, pause for 1000ms(1sec)
	j 		replay_note		
	#play the indivual note
	replay_note:
		#syscall to play the specific note from the file
		move	$a0, $t4		
		li	$a1, 1000		
		li	$a2, 25
		li	$a3, 50
		li	$v0, 31
		syscall

		#pause between notes to prevent all playing immediately
		li	$a0, 400
		li	$v0, 32
		syscall
		j	replay_loop
replay_pause:
	#loading bar bitmap
	move	$a0, $t3
	li	$a1, 125
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 126
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 127
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 128
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 129
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 130
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 131
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 132
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 133
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 134
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 135
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 136
	lw	$a2, load_color
	move	$a0, $t3
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 137
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 138
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 139
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 140
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 141
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 142
	lw	$a2, load_color
	jal DrawPoint
	add	$t3, $t3, 1
	move	$a0, $t3
	li	$a1, 125
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 126
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 127
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 128
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 129
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 130
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 131
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 132
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 133
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 134
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 135
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 136
	lw	$a2, load_color
	move	$a0, $t3
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 137
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 138
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 139
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 140
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 141
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 142
	lw	$a2, load_color
	jal DrawPoint
	add	$t3, $t3, 1
	move	$a0, $t3
	li	$a1, 125
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 126
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 127
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 128
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 129
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 130
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 131
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 132
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 133
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 134
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 135
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 136
	lw	$a2, load_color
	move	$a0, $t3
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 137
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 138
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 139
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 140
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 141
	lw	$a2, load_color
	jal DrawPoint
	move	$a0, $t3
	li	$a1, 142
	lw	$a2, load_color
	jal DrawPoint
	#syscall to pause for 1000ms(1s)
	#used for the space character in the files
	li		$a0, 1000
	li		$v0, 32
	syscall

	j		replay_loop

# $a0 contains x position
#$a1 contains y position
#$a2 contains the color	
DrawPoint:
		sll $t0, $a1, 9   # multiply y-coordinate by 64 (length of the field)
		addu $v0, $a0, $t0
		sll $v0, $v0, 2
		addu $v0, $v0, $gp
		sw $a2, ($v0)		# draw the color to the location
		jr $ra

#Records time between key presses in ms
#This is used to record pauses in between notes played
time_start:
	li		$v0, 30
	syscall
	
	move		$t6, $a0
	
main_loop:
	#Checks to see if there has been a keystroke
	#--------------------------------------------------------------------
	#if there has been one, then the least significant bit of 0xFFFF0000
	#will be turned to 1 and branch to keypress to process the note
	lw		$t2, 0xFFFF0000		#Load receiver control register into $t2
	andi		$t2, $t2, 0x01
	bne		$t2, 0, keypress	#if lsb of $t2 is 1, then branch to keypress
	
	add		$t3, $t3, 1
	
	j 		main_loop
#Used for processing keypresses
#Stores the keypress, remaps the key, and plays the midi
keypress:
	#Calculate the end time between keystrokes
	li		$v0, 30
	syscall
	move		$t7, $a0
	
	#subtract the end time by the starting time to calculate the total time between keystrokes
	subu		$t7, $t7, $t6
	divu		$a0, $t7, 1000
	la		$a1, output_buffer
	jal		add_spaces
	
	#Save keystroke data into $t1
	#if esc is pressed, branch to save_song to save the song and exit the program
	lw	$t1, 0xFFFF0004			
	beq	$t1, 0x0000001b, save_song
	
	#move the keystroke data into $a0
	#call bind_key to remap the keypress
	#keys are remapped to a layout more similar to a traditional keyboard
	move	$a0, $t1
	jal	bind_key
	
	#60 is subtracted from the keystroke data to change the octave. This can be changed by multiples of 12.
	#The new keystroke data (with new octave) is stored into key
	sub	$a0, $a0, 60
	sw	$a0, key			
	move	$a0, $t1
	
	#loads the output_buffer and key to call the store_key function
	la	$a0, output_buffer
	lw	$a1, key
	jal	store_key
	
	#Syscall for midi output
	#$a0: data of sound to be played
	#$a1: time of sound played
	#$a2: instrument
	#$a3: volume
	lw	$a0, key			
	li	$a1, 1000
	li	$a2, 25
	li	$a3, 50
	li	$v0, 31
	syscall
	
	#$t1 is reset to 0
	#is s1(replay flag) is set to 0, then branch to replay loop to read another byte
	#jump to time_start to process another keypress
	li	$t1, 0			
	beq	$s1, 0, replay_loop	
	j time_start

#Add spaces to the output buffer for pauses between notes played			
add_spaces:
	#load i and 'space' character into registers
	li	$t1, 0	#i
	li	$t3, 0x00000020	#load 'space' hex into t3
	
	#loop adds a space into the output buffer on each iteration
	#loops the amount of spaces calculated previously
	add_spaces_loop:
	beq	$t1, $a0, add_spaces_exit
	lb	$t2, ($a1)
	beq	$t2, 0, space_to_output
	addi	$a1, $a1, 1
	j	add_spaces_loop
	
	#stores the space to the output_buffer
	space_to_output:
	sb	$t3, ($a1)
	addi	$t1, $t1, 1
	addi	$a1, $a1, 1
	j	add_spaces_loop
	#return function
	add_spaces_exit:
	jr	$ra
#Stores the key into the output buffer
store_key:
	#loop until an empty space is found
	#then input the key into the empty space
	lb	$t4, ($a0)
	beq	$t4, 0, key_to_output
	addi	$a0, $a0, 1 
	j store_key
	#store the key to the output buffer and return function
	key_to_output:
	sb	$a1, ($a0)
	jr	$ra

#function used to remap the keys into a layout
#more similar to a standard keyboard
bind_key:
	beq	$a0, 0x00000077, bind_w		
	beq	$a0, 0x00000073, bind_s		
	beq	$a0, 0x00000065, bind_e		
	beq	$a0, 0x00000066, bind_f		
	beq	$a0, 0x00000074, bind_t		
	beq	$a0, 0x00000067, bind_g		
	beq	$a0, 0x00000079, bind_y		
	beq	$a0, 0x00000068, bind_h		
	beq	$a0, 0x00000075, bind_u		
	beq	$a0, 0x0000006a, bind_j		
	beq	$a0, 0x0000006b, bind_k		
	jr	$ra
	#bind the w key to 61
	bind_w: 
	add	$a0, $a0, $zero
	li 	$a0, 0x00000061
	jr	$ra
	#bind the s key to 62
	bind_s:
	add	$a0, $a0, $zero
	li 	$a0, 0x00000062
	jr	$ra
	#bind e key to 63
	bind_e:
	add	$a0, $a0, $zero
	li 	$a0, 0x00000063
	jr	$ra
	#bind f key to 65
	bind_f:
	add	$a0, $a0, $zero
	li 	$a0, 0x00000065
	jr	$ra
	#bind t key to 66
	bind_t:
	add	$a0, $a0, $zero
	li 	$a0, 0x00000066
	jr	$ra
	#bind g key to 67
	bind_g:
	add	$a0, $a0, $zero
	li 	$a0, 0x00000067
	jr	$ra
	#bind y key to 68
	bind_y:
	add	$a0, $a0, $zero
	li 	$a0, 0x00000068
	jr	$ra
	#bind h key to 69
	bind_h:
	add	$a0, $a0, $zero
	li 	$a0, 0x00000069
	jr	$ra
	#bind u key to 70
	bind_u:
	add	$a0, $a0, $zero
	li 	$a0, 0x00000070
	jr	$ra
	#bind j key to 71
	bind_j:
	add	$a0, $a0, $zero
	li 	$a0, 0x00000071
	jr	$ra
	#bind k key to 72
	bind_k:
	add	$a0, $a0, $zero
	li 	$a0, 0x00000072
	jr	$ra
#Saves the song from the output buffer into a file
save_song:
	#dialog asking to name the save file
	la	$a0, save_dialog
	la	$a1, save_name
	li	$a2, 9
	li	$v0, 54
	syscall
	
	#Open file save_name
	li   $v0, 13       	# system call for open file
  	la   $a0, save_name   	# output file name
	li   $a1, 1        	# Open for writing (flags are 0: read, 1: write)
 	li   $a2, 0        	# mode is ignored
 	syscall            	# open a file (file descriptor returned in $v0)
 	move $s6, $v0     	# save the file descriptor
 	
 	#Write the output_buffer to the save_name file
  	li   $v0, 15      	 # system call for write to file
  	move $a0, $s6     	 # file descriptor 
  	la   $a1, output_buffer  # address of buffer from which to write
  	li   $a2, 100      	 # hardcoded buffer length
  	syscall           	 # write to file
  	
exit:
	li		$v0, 10
	syscall
